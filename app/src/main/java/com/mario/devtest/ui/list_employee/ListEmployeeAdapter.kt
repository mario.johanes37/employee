package com.mario.devtest.ui.list_employee

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mario.devtest.R
import com.mario.devtest.core.model.ListEmployeeResponse
import com.mario.devtest.databinding.ItemEmployeeBinding

class ListEmployeeAdapter : RecyclerView.Adapter<ListEmployeeAdapter.ListEmployeeHolder>() {
    var onClickEdit: ((ListEmployeeResponse.Data) -> Unit)? = null
    var onClickDelete: ((ListEmployeeResponse.Data) -> Unit)? = null


    private val diffCallBack = object : DiffUtil.ItemCallback<ListEmployeeResponse.Data>() {
        override fun areItemsTheSame(
            oldItem: ListEmployeeResponse.Data,
            newItem: ListEmployeeResponse.Data
        ): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(
            oldItem: ListEmployeeResponse.Data,
            newItem: ListEmployeeResponse.Data
        ): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, diffCallBack)

    inner class ListEmployeeHolder(private val binding: ItemEmployeeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int, data: ListEmployeeResponse.Data) {
            binding.apply {
                if (position % 2 == 0) {
                    root.setBackgroundColor(ContextCompat.getColor(root.context, R.color.white))
                }
                tvName.text = data.employeeName
                tvSalary.text = data.employeeSalary
                tvAge.text = data.employeeAge
                btnEditEmployee.setOnClickListener {
                    onClickEdit?.invoke(data)
                }
                btnDeleteEmployee.setOnClickListener {
                    onClickDelete?.invoke(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListEmployeeAdapter.ListEmployeeHolder {
        return ListEmployeeHolder(
            ItemEmployeeBinding.bind(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_employee, parent, false
                )
            )
        )
    }

    override fun onBindViewHolder(holder: ListEmployeeAdapter.ListEmployeeHolder, position: Int) {
        holder.bind(position, differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size
}