package com.mario.devtest.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mario.devtest.core.base.BaseViewModel
import com.mario.devtest.core.datasource.ApiState
import com.mario.devtest.core.datasource.EmployeeRepository
import com.mario.devtest.core.model.BodyCreateEmployee
import com.mario.devtest.core.model.ListEmployeeResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class EmployeeViewModel @Inject constructor(private val repository: EmployeeRepository) :
    BaseViewModel() {
    private val _listEmployeeResponseResult =
        MutableLiveData<ApiState<ListEmployeeResponse>>()
    val listEmployeeResponseResult: LiveData<ApiState<ListEmployeeResponse>> =
        _listEmployeeResponseResult

    private val _postCreateEmployeeResult =
        MutableLiveData<ApiState<Unit>>()
    val postCreateEmployeeResult: LiveData<ApiState<Unit>> =
        _postCreateEmployeeResult

    private val _putUpdateEmployeeResult =
        MutableLiveData<ApiState<Unit>>()
    val postCreateEmployee: LiveData<ApiState<Unit>> =
        _putUpdateEmployeeResult


    fun getListEmployee() {
        viewModelScope.launch(Dispatchers.IO) {
            _listEmployeeResponseResult.postValue(ApiState.loading())
            try {
                val response = repository.getListEmployee()
                _listEmployeeResponseResult.postValue(response)
            } catch (e: Exception) {
                onError.postValue(e.message)
            }
        }
    }

    fun postCreateEmployee(body: BodyCreateEmployee) {
        viewModelScope.launch(Dispatchers.IO) {
            _listEmployeeResponseResult.postValue(ApiState.loading())
            try {
                val response = repository.postCreateEmployee(body)
                _postCreateEmployeeResult.postValue(response)
            } catch (e: Exception) {
                onError.postValue(e.message)
            }
        }
    }

    fun putUpdateEmployee(id: Int, body: BodyCreateEmployee) {
        viewModelScope.launch(Dispatchers.IO) {
            _listEmployeeResponseResult.postValue(ApiState.loading())
            try {
                val response = repository.putUpdateEmployee(id, body)
                _postCreateEmployeeResult.postValue(response)
            } catch (e: Exception) {
                onError.postValue(e.message)
            }
        }
    }
}