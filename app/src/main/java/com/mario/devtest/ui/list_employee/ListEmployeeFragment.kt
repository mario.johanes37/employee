package com.mario.devtest.ui.list_employee

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mario.devtest.core.base.BaseFragment
import com.mario.devtest.core.base.State
import com.mario.devtest.core.di.builder.EmployeeDaggerWrapper
import com.mario.devtest.core.model.BodyCreateEmployee
import com.mario.devtest.core.model.ListEmployeeResponse
import com.mario.devtest.databinding.FragmentListEmployeeBinding
import com.mario.devtest.ui.viewmodel.EmployeeViewModel
import javax.inject.Inject

class ListEmployeeFragment : BaseFragment<FragmentListEmployeeBinding>() {
    private lateinit var listEmployeeAdapter: ListEmployeeAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<EmployeeViewModel> { viewModelFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EmployeeDaggerWrapper.getComponent().inject(this@ListEmployeeFragment)
    }

    override fun setViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentListEmployeeBinding =
        FragmentListEmployeeBinding.inflate(layoutInflater, null, false)


    override fun setUpVariable() {
        context?.let {
            viewModel.getListEmployee()
            setupObserve(it)
        }
    }

    private fun setupAdapter(context: Context, data: ListEmployeeResponse) {
        binding.apply {
            listEmployeeAdapter = ListEmployeeAdapter()
            rvListEmployee.layoutManager = LinearLayoutManager(context)
            rvListEmployee.adapter = listEmployeeAdapter
            listEmployeeAdapter.differ.submitList(data.data)
            listEmployeeAdapter.onClickEdit = {
                val body = BodyCreateEmployee("", "", "")
                viewModel.putUpdateEmployee(it.id, body)
            }
            listEmployeeAdapter.onClickDelete = {

            }
        }
    }

    private fun setupObserve(context: Context) {
        viewModel.listEmployeeResponseResult.observe(viewLifecycleOwner) { data ->
            when (data.status) {
                State.LOADING -> {}
                State.SUCCESS -> {
                    data.data?.let {
                        setupAdapter(context, it)
                    }
                }

                State.ERROR -> {

                }
            }
        }
    }
}