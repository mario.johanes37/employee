package com.mario.devtest.ui.dashboard_employee

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mario.devtest.R
import com.mario.devtest.core.base.BaseFragment
import com.mario.devtest.core.base.State
import com.mario.devtest.core.di.builder.EmployeeDaggerWrapper
import com.mario.devtest.core.model.BodyCreateEmployee
import com.mario.devtest.databinding.FragmentDashboardEmployeeBinding
import com.mario.devtest.ui.viewmodel.EmployeeViewModel
import javax.inject.Inject

class DashboardEmployeeFragment : BaseFragment<FragmentDashboardEmployeeBinding>() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<EmployeeViewModel> { viewModelFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EmployeeDaggerWrapper.getComponent().inject(this@DashboardEmployeeFragment)
    }

    override fun setViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentDashboardEmployeeBinding =
        FragmentDashboardEmployeeBinding.inflate(layoutInflater, null, false)


    override fun setUpVariable() {
        setupObserve()
        binding.apply {
            btnSubmitEmployee.setOnClickListener {
                val body = BodyCreateEmployee(
                    etAgeEmployee.text.toString(),
                    etNameEmployee.text.toString(),
                    etSalaryEmployee.text.toString()
                )
                viewModel.postCreateEmployee(body)
            }
            btnShowEmployee.setOnClickListener {
                findNavController().navigate(R.id.action_dashboardEmployeeFragment_to_listEmployeeFragment)
            }
        }
    }

    private fun setupObserve() {
        binding.apply {
            viewModel.postCreateEmployeeResult.observe(viewLifecycleOwner) {
                when (it.status) {
                    State.LOADING -> {}
                    State.SUCCESS -> {}
                    State.ERROR -> {}
                }
            }
        }
    }
}