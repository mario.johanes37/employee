package com.mario.devtest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.mario.devtest.R
import com.mario.devtest.core.base.BaseActivity
import com.mario.devtest.core.di.builder.EmployeeDaggerWrapper
import com.mario.devtest.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding>() {
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        EmployeeDaggerWrapper.buildComponent(this.applicationContext)
        super.onCreate(savedInstanceState)
    }

    override fun setViewBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun setUpVariable() {
        binding.apply {
            navController = Navigation.findNavController(this@MainActivity, R.id.container_employee)

        }
    }
}