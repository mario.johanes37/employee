package com.mario.devtest.core.di

import android.content.Context
import com.mario.devtest.core.di.annotation.FeatureScope
import com.mario.devtest.core.di.network.NetworkModule
import com.mario.devtest.core.di.viewmodel.ViewModelModule
import com.mario.devtest.ui.dashboard_employee.DashboardEmployeeFragment
import com.mario.devtest.ui.list_employee.ListEmployeeFragment
import dagger.BindsInstance
import dagger.Component

@FeatureScope
@Component(modules = [NetworkModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(listEmployeeFragment: ListEmployeeFragment)
    fun inject(dashboardEmployeeFragment: DashboardEmployeeFragment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindContext(context: Context): Builder

        fun build(): ApplicationComponent
    }
}