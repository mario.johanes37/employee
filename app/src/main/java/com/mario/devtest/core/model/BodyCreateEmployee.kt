package com.mario.devtest.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BodyCreateEmployee(
    @SerializedName("name") var name: String,
    @SerializedName("salary") var salary: String,
    @SerializedName("age") var age: String
) : Serializable