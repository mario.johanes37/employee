package com.mario.devtest.core.datasource

import com.mario.devtest.core.model.BodyCreateEmployee
import com.mario.devtest.core.model.ListEmployeeResponse
import javax.inject.Inject

class EmployeeRepository @Inject constructor(private val apiService: ApiService) {
    suspend fun getListEmployee(): ApiState<ListEmployeeResponse> {
        apiService.getListEmployee().let {
            return ApiState.returnData(it)
        }
    }

    suspend fun postCreateEmployee(body: BodyCreateEmployee): ApiState<Unit> {
        apiService.postCreateEmployee(body).let {
            return ApiState.returnData(it)
        }
    }

    suspend fun putUpdateEmployee(id: Int, body: BodyCreateEmployee): ApiState<Unit> {
        apiService.putUpdateEmployee(id, body).let {
            return ApiState.returnData(it)
        }
    }
}