package com.mario.devtest.core.di.builder

import android.content.Context
import com.mario.devtest.core.di.ApplicationComponent
import com.mario.devtest.core.di.DaggerApplicationComponent

object EmployeeDaggerWrapper {
    private var component: ApplicationComponent? = null

    fun getComponent(): ApplicationComponent {
        assert(component != null)
        return component!!
    }

    fun buildComponent(context: Context) {
        component = DaggerApplicationComponent.builder().bindContext(context).build()
    }

    fun clearComponent() {
        component = null
    }
}