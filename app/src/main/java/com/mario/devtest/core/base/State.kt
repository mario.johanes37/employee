package com.mario.devtest.core.base

enum class State {
    LOADING, SUCCESS, ERROR
}