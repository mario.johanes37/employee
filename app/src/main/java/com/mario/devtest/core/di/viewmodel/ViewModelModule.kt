package com.mario.devtest.core.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mario.devtest.core.di.annotation.ViewModelKey
import com.mario.devtest.core.viewmodel_factory.ViewModelFactory
import com.mario.devtest.ui.viewmodel.EmployeeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(EmployeeViewModel::class)
    abstract fun bindGenreViewModel(employeeViewModel: EmployeeViewModel): ViewModel

    @Binds
    abstract fun bindAboutViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}