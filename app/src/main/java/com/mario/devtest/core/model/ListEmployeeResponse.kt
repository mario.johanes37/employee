package com.mario.devtest.core.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ListEmployeeResponse(
    @SerializedName("status") var status: String = "",
    @SerializedName("data") var data: ArrayList<Data> = arrayListOf()
) : Serializable {
    data class Data(
        @SerializedName("id") var id: Int = 0,
        @SerializedName("employee_name") var employeeName: String = "",
        @SerializedName("employee_salary") var employeeSalary: String = "",
        @SerializedName("employee_age") var employeeAge: String = "",
    ) : Serializable
}
