package com.mario.devtest.core.datasource

import com.mario.devtest.core.model.BodyCreateEmployee
import com.mario.devtest.core.model.ListEmployeeResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiService {
    @GET("api/v1/employees")
    suspend fun getListEmployee(): Response<ListEmployeeResponse>

    @POST("api/v1/create")
    suspend fun postCreateEmployee(@Body body: BodyCreateEmployee): Response<Unit>

    @PUT("api/v1/update/{id}")
    suspend fun putUpdateEmployee(
        @Path("id") idEmployee: Int,
        @Body body: BodyCreateEmployee
    ): Response<Unit>
}